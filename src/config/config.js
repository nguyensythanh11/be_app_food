process // Biến toàn cục chứa thông tin hệ thống và môi trường

import dotenv from 'dotenv';
dotenv.config();

export default {
    datbase: process.env.DB_DATABASE,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT,
    port: process.env.DB_PORT
}