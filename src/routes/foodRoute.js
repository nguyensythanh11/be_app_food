import express from 'express';
import { createEvaluate, createOrder, getLike, getRate, likeFood } from '../controller/foodController.js';

const foodRoute = express.Router();

// Xử lý like 
foodRoute.put('/like-food', likeFood);
// Lấy danh sách like theo nhà hàng và user
foodRoute.get('/get-like',getLike);
// Xử lý dánh giá
foodRoute.post('/create-evaluate', createEvaluate);
// Lấy danh giá theo nhà hàng và user
foodRoute.get('/get-rate',getRate);
// Đặt món
foodRoute.post('/create-order',createOrder)

export default foodRoute;

