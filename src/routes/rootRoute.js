import express from 'express';
import foodRoute from './foodRoute.js';

const rootRoute = express.Router();

rootRoute.use('/food', foodRoute);

export default rootRoute;