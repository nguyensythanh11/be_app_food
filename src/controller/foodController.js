import initModels from "../models/init-models.js";
import sequelize from '../models/connect.js'

const model = initModels(sequelize);

const likeFood = async (req,res) => {
    try {
        const { userId, resId } = req.body;

        let data = await model.like_res.findOne({
            where: {
                user_id: userId,
                res_id: resId
            }
        })
        if(!data){
            await model.like_res.create({
                user_id: userId,
                res_id: resId,
                date_like: new Date()
            });
            res.status(200).json({ success: 'Success to like the restaurant'});
        } else {
            await model.like_res.destroy({
                where: {
                    user_id: userId,
                    res_id: resId
                }
            })
            res.status(200).json({ success: 'Success to unlike the restaurant' })
        } 
    } catch (error) {
        res.status(500).json({ error: 'Failed to like the restaurant'});
    }
}

const getLike = async (req,res) => {
    try {
        let { userId, resId } = req.body;
        let data = await model.like_res.findAll({
            where: {
                user_id: userId,
                res_id: resId
            }
        })
        res.status(200).send(data);
    } catch (error) {
        res.status(500).json({error: 'Failed to get like the restaurant and user'})
    }
}

const createEvaluate = async (req,res) => {
    try {
        let { userId, resId, amount } = req.body;
        let checkExist = await model.rate_res.findOne({
            where: {
                user_id: userId,
                res_id: resId
            }
        })
        if(checkExist){
            res.send('Bạn đã đánh giá nhà hàng này rồi')
        }
        else {
            await model.rate_res.create({
                user_id: userId,
                res_id: resId,
                amount,
                date_rate: new Date()
            });
            res.send('Đánh giá thành công')
        }
    } catch (error) {
        res.status(500).json({error: 'Failed'})
    }
}

const getRate = async (req,res) => {
    try {
        let { user_id, res_id } = req.body;
        let data = await model.rate_res.findOne({
            where: {
                user_id,
                res_id
            },
            include: ["re","user"]
        })
        res.send(data);

    } catch (error) {
        res.send(error)
    }
}

const createOrder = async (req,res) => {
    try {
        let { user_id,food_id,amount,arr_sub_id } = req.body;
        await model.orders.create({
            user_id,
            food_id,
            amount,
            code: "",
            arr_sub_id
        })
        res.send('Thêm món thành công');
    } catch (error) {
        res.send('Thêm món thất bại');
    }
}

export { likeFood,getLike,createEvaluate,getRate,createOrder };